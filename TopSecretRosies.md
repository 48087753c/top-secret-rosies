# <font style="color:red">Top Secret Rosies</font>
Top Secret Rosies es el nombre que se le da a un grupo de mujeres que en 1942 fueron contratadas por el ejército Estadounidense para programar uno de los primeros computadores, el [ENIAC](https://blogs.imf-formacion.com/blog/tecnologia/proyecto-eniac-la-historia-de-un-gigante-del-hardware-202302/).

El papel de estas mujeres fue muy importante para el desarrollo de la Segunda Guerra Mundial ya que el ENIAC fue un computador de vital importancia para la época, ya que realizó los cálculos matemáticos que sirvieron para mejorar los ataques y gracias a estos los aliados pudieron poner fin a la guerra antes debido a la eficiencia de los cálculos.

<img align=centre witdh="200" height=300 src="https://www.conectadasextremadura.org/wp-content/uploads/2022/06/img-mujeres-referentes-01-1.jpg">

### ¿Quien forma este grupo?
* Jean Bartik
* Ruth Teitelbaum
* Marlyn Meltzer
* Betty Holberton
* Frances Spence
* Kathleen Antonelli

#### Aportaciones
La presentación del ENIAC fue un hecho que dio a conocer a sus constructores ingenieros John Presper Eckert y John William Mauchly. Sin embargo el trabajo de programar el computador realizado por este grupo de mujeres fue silenciado. Se decia que las mujeres que aparecian en las imágenes eran modelos. Mucho tiempo después se reconocería su gran aportación gracias al documental, [__Top Secret Rosies: The Female Computers of WWII__](https://www.youtube.com/watch?v=bGk9W65vXNA).

A lo largo del tiempo cada integrante de este equipo ha estado presente en diferentes proyectos para el desarollo y evolución de la programación.

----

#### Betty Holberton (1917-2001)
 Entre ellas destaca especialmente Betty Snyder Holberton, quien participó en el desarrollo del __UNIVAC I__, que fue el primer ordenador que se controlaba a través de instrucciones introducidas mediante teclado. Ella también contribuyó en el desarrollo del __C-10__, un prototipo de los modernos lenguajes de programación. Con Grace Hopper, Holberton participó en la creación de los primeros estándares para __COBOL__ y __FORTRAN__. 
 
<img align="right" width="200" src="https://upload.wikimedia.org/wikipedia/commons/b/bd/Betty-Holbeton-circa-1950s-600dpi.jpg"> 


<br>










<br>


<font size="6">*“Betty tenía una mente lógica asombrosa y resolvía más problemas mientras dormía que otras personas despiertas.” — Jean J. Bartik*<font/>

<br>



<br>



<br>



<br>





<br>






<br>





<br>





<br>
